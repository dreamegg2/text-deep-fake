import torch
from loguru import logger as info_logger
from src.disk import disk
from pathlib import Path
from src.losses.vgg import VGGLoss
from src.data.baseline import GenBaselineDataset
from src.losses.vgg import VGGLoss
from src.utils.download import download_dataset
from src.models.embedders import ContentResnet, StyleResnet
from src.models.nlayer_discriminator import NLayerDiscriminator
from src.models.stylegan import StyleBased_Generator
from src.training.stylegan_adversarial_o import StyleGanAdvTrainer
from src.storage.simple import Storage
from src.losses.STRFL import OCRLoss
from src.losses.typeface_perceptual import TypefacePerceptualLoss
from torch.utils.data import DataLoader


class Config:
    def __init__(self):
        device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
        info_logger.info(f'Using device: {device}')
        style_dir = Path('/home/ckmoon/clab/Datasets/')
        
        batch_size = 4*4
        train_dataloader = DataLoader(GenBaselineDataset(style_dir / '4paired_20k', return_style_labels=True),
                                      shuffle=True, batch_size=batch_size, num_workers=4, drop_last=True)
        val_dataloader = DataLoader(GenBaselineDataset(style_dir / '4paired_20', return_style_labels=True), batch_size=batch_size, num_workers=4)

        total_epochs = 500


        model_G = StyleBased_Generator(dim_latent=512)
        # model_G.load_state_dict(torch.load(f'{weights_folder}/model'))
        model_G.to(device)

        style_embedder = StyleResnet().to(device) 
        # style_embedder.load_state_dict(torch.load(f'{weights_folder}/style_embedder'))

        content_embedder = ContentResnet().to(device)
        # content_embedder.load_state_dict(torch.load(f'{weights_folder}/content_embedder'))

        model_D = NLayerDiscriminator(input_nc=3, ndf=64, n_layers=3, norm_layer=(lambda x : torch.nn.Identity()))
        model_D.to(device)

        optimizer_G = torch.optim.AdamW(
            list(model_G.parameters()) +
            list(style_embedder.parameters()) +
            list(content_embedder.parameters()),
            lr=1e-3,
            weight_decay=1e-6
        )
        scheduler_G = torch.optim.lr_scheduler.ExponentialLR(
            optimizer_G,
            gamma=0.9
        )

        optimizer_D = torch.optim.AdamW(model_D.parameters(), lr=1e-4)
        scheduler_D = torch.optim.lr_scheduler.ExponentialLR(
            optimizer_D,
            gamma=0.9
        )

        ocr_coef = 0.07
        cycle_coef = 2.0
        recon_coef = 2.0
        emb_coef = 0.0
        perc_coef = 25.0
        tex_coef = 7.0
        adv_coef = 0.06

        self.trainer = StyleGanAdvTrainer(
            model_G,
            model_D,
            style_embedder,
            content_embedder,
            optimizer_G,
            optimizer_D,
            scheduler_G,
            scheduler_D,
            train_dataloader,
            val_dataloader,
            None, # storage,
            None, # logger,
            total_epochs,
            device,
            ocr_coef,
            cycle_coef,
            recon_coef,
            emb_coef,
            perc_coef,
            tex_coef,
            adv_coef,
            OCRLoss(),
            TypefacePerceptualLoss(),
            VGGLoss(),
            torch.nn.L1Loss(),
            torch.nn.MSELoss()
        )

    def run(self):
        self.trainer.run()
